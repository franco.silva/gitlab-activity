import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { GitlabAuthPageComponent } from './pages/gitlab-auth-page/gitlab-auth-page.component';
import { MainPageComponent } from './pages/main-page/main-page.component';

const routes: Route[] = [
  {
    path: '',
    component: MainPageComponent
  },
  {
    path: 'gitlab_success',
    component: GitlabAuthPageComponent
  }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
