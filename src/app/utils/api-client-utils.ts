export class ApiClientUtils {
    public static toQueryString(query?: {[key: string]: any}): string {
        if (!query)  { return ''; }
        const keys = Object.keys(query);
        const components = keys.map(key => {
        let value = query[key];
        //   if (value instanceof Date) {
        //     value = getUnixTimestamp(value, true);
        //   }
        if (value instanceof Array) {
            return value.map(v => `${key}=${encodeURIComponent(v)}`).join('&');
        }
            return `${encodeURIComponent(key)}=${encodeURIComponent(value)}`;
        });
        return components.length > 0 ? '?' + components.join('&') : '';
      }
}