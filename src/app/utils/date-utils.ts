import { addDays, subDays } from "date-fns";

export class DateUtils {

    public static getLastWeekMonday(): Date {
        const now = new Date();
        const lastMonday = subDays(now, 7)
        lastMonday.setDate(lastMonday.getDate() - (lastMonday.getDay() + 6) % 7);
        return lastMonday;
    }

    public static getLastWeekFriday(): Date {
        return addDays(DateUtils.getLastWeekMonday(), 4)
    }

    /**
     * Given a weekday [1-7] and taking into account last week's monday returns a Date object
     * @returns a date 
     */
    public static getDayFromWeekDayGivenLastWeekMonday(weekday: number): Date{
        const lastMonday = this.getLastWeekMonday(); // this will be day 1 (if I want Wed, I will pass in 3 and the expected result is monday + 3 days - 1)
        lastMonday.setHours(0, 0, 0);
        return addDays(lastMonday, weekday - 1);
    }

}