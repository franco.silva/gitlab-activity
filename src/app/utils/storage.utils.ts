import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class StorageUtils {

    private storage = window.localStorage;

    constructor() {
        this.isLocalStorageAvailable();
    }

    isLocalStorageAvailable(): boolean {
        const testLocalStorageKey = 'rm-test-local-storage-key';
        try {
          if(this.storage.getItem(testLocalStorageKey)) {
            return true;
          }
          this.storage.setItem(testLocalStorageKey, '1');
          this.storage.removeItem(testLocalStorageKey);
          return true;
        } catch(e) {
          console.warn('Local storage is not available for configuration saving.')
          return false;
        }
    }
    
    save(key: string, data: any): void {
        this.storage.setItem(key, JSON.stringify(data));
        
    }
    
    read(key: string): any | undefined {
        const data = this.storage.getItem(key);
        try {
          return data ? JSON.parse(data) : undefined;
        } catch(e) {
          console.warn(e);
        }        
    }

    delete(key: string): void {
      this.storage.removeItem(key);
    }
}