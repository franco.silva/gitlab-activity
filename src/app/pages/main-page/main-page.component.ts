import { Component, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { RmUiModalService } from '@rabbit-mobile/rm-ui';
import { addDays, format, subDays } from 'date-fns';
import { take } from 'rxjs';
import { GitlabApiClient } from 'src/app/modules/api-client/gitlab/gitlab-api-client';
import { GitlabEvent } from 'src/app/modules/api-client/gitlab/models/event';
import { GitlabProject } from 'src/app/modules/api-client/gitlab/models/project';
import { ProadTimeReg } from 'src/app/modules/api-client/proad/models/timereg';
import { ProjectConfigurationService } from 'src/app/modules/services/project-configuration.service';
import { EventsListComponent } from 'src/app/modules/shared/events-list/events-list.component';
import { DateUtils } from 'src/app/utils/date-utils';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent {

  lastWeekMonday = DateUtils.getLastWeekMonday();
  lastWeekFriday = DateUtils.getLastWeekFriday();
  // limit the fetch only from last monday    
  intendedDatePeriod = {
    after: this.lastWeekMonday,
    before: this.lastWeekFriday
  }
  hasAccessToken?: boolean;
  gitlabEvents: GitlabEvent[] = [];
  gitlabProjects: GitlabProject[] = [];
  form?: FormGroup;
  dateRangeForm = new FormGroup({
    dates: new FormControl([this.intendedDatePeriod.after, this.intendedDatePeriod.before])
  })
  projectsForm?: FormGroup;
  isLoading = false;

  proadProjectsMap: {
    gitlabProjectId: number,
    proadProjectMilestone: string
  }[] = [];

  hardCodedWeekdays = [
    { dayOfWeek: 1, label : 'Mon' },
    { dayOfWeek: 2, label : 'Tue' },
    { dayOfWeek: 3, label : 'Wed' },
    { dayOfWeek: 4, label : 'Thu' },
    { dayOfWeek: 5, label : 'Fri' },
  ];

  get readableDatesFromParams(): { before: Date, after: Date } {
    return {
      before: this.intendedDatePeriod.before,
      after: this.intendedDatePeriod.after
    }
  }

  constructor(
    private gitlabApiClient: GitlabApiClient,
    private projectConfigService: ProjectConfigurationService,
    private modalService: RmUiModalService
  ) {

    this.hasAccessToken = this.gitlabApiClient.hasAccessToken;

    // load projects map configuration from localStorage if set
    this.projectConfigService.configuration$.subscribe(projects => {
      this.proadProjectsMap = projects;
    });

    this.dateRangeForm.get('dates')?.valueChanges.subscribe((range) => {
      if(range?.length === 2) {
        const after = range[0];
        const before = range[1];
        this.intendedDatePeriod = {
          after,
          before
        }
      }
      this.getActivity();
    })
    
    this.getActivity();
    
  }

  getActivity(): void {

    // the api expexts a YYYY-MM-DD format
    const queryParams = {
      after: format(subDays(this.intendedDatePeriod.after, 1), 'yyyy-MM-dd'), // the api does not include the specified day, so we must specify the previous day for the period we want to see
      before: format(addDays(this.intendedDatePeriod.before, 1), 'yyyy-MM-dd'),
    }
          
    if(this.gitlabApiClient.hasAccessToken) {

      this.isLoading = true;

      this.gitlabApiClient.getUserActivity(environment.gitlab.userId, queryParams).pipe(take(1)).subscribe({
        next: (data) => {
          this.gitlabEvents = data[0];
          this.gitlabProjects = data[1];
          this.initWeekMapForm();
          this.initProjectsForm(this.gitlabProjects);
          this.isLoading = false;
        },
        error: () => {
          this.isLoading = false;
        }
      });
    }
  }

  

  

  getProadMilestoneFromProjectId(id: number): string | undefined {
    return this.proadProjectsMap.find(p => p.gitlabProjectId === id)?.proadProjectMilestone ?? '';
  }

  /**
   * returns projects for a given weekday
   * @param weekday - number from 1 to 7 where 1 is Monday
   * @returns GitlabProject[]
   */
  getProjectsWorkedOnWeekday(weekday: number): GitlabProject[] {

    const events = this.gitlabEvents.filter(event => {
      const eventDate = new Date(event?.created_at!);
      return eventDate.getDay() === weekday;
    });
    const projectEvents = events.map(e => e.project_id);
    const projects = this.gitlabProjects.filter(p => projectEvents.includes(p?.id)).filter((p): p is GitlabProject => !!p).sort((a, b) => a.name > b.name ? 1 : -1);
    return projects;
  }

  getDateForWeekDayFromLastMonday(weekday: number): Date {
    return addDays(this.lastWeekMonday, weekday);
  }

  getDateForWeekDayForCurrentRange(weekday: number): Date | undefined {
    // fill array with all dates
    const allDatesBetweenRange: Date[] = [];
    const range = this.intendedDatePeriod;
    // Start from 'after' date and iterate until 'before' date
    let currentDate = new Date(range.after);
    while (currentDate <= range.before) {
      allDatesBetweenRange.push(new Date(currentDate)); // Push a new date object to the array to avoid reference issues
      currentDate.setDate(currentDate.getDate() + 1); // Move to the next day
    }
    return allDatesBetweenRange.find(date => date.getDay() === weekday);
  }
  
  getActivityForProjectOnWeekday(project: GitlabProject, weekday: number): GitlabEvent[] {

    const allActivityForProject = this.gitlabEvents.filter(e => e.project_id === project.id).filter(e => {
      if(e.created_at) {
        const activityDate = new Date(e.created_at);
        return activityDate.getDay() === weekday
      }
      return false;      
    });
    console.log(allActivityForProject);
    return allActivityForProject;
  }

  didPressProjectActivityForWeekday(project: GitlabProject, weekday: number): void {
    const activityForProject = this.getActivityForProjectOnWeekday(project, weekday);
    this.modalService.custom({
      component: EventsListComponent,
      componentParams: {
        projects: this.gitlabProjects,
        events: activityForProject,
        project,
        detailed: true
      },
      classNames: 'project-activity-modal'
    });
  }

  initProjectsForm(projects: (GitlabProject | undefined)[]): void {
    this.projectsForm = new FormGroup({});
    (projects || []).forEach(p => {
      const proadMileStone = this.getProadMilestoneFromProjectId(p?.id!)
      this.projectsForm?.addControl(p?.id?.toString()!, new FormControl(proadMileStone));
    })

  }
  
  initWeekMapForm(): void {

    // TODO: this could be dynamic for more weekdays? stick with 5 day week
    const projectsForMonday = this.getProjectsWorkedOnWeekday(1);
    const projectsForTuesday = this.getProjectsWorkedOnWeekday(2);
    const projectsForWednesday = this.getProjectsWorkedOnWeekday(3);
    const projectsForThursday = this.getProjectsWorkedOnWeekday(4);
    const projectsForFriday = this.getProjectsWorkedOnWeekday(5);

    this.form = new FormGroup({
      // each is a week day, containing an array of projects
      1 : new FormArray(projectsForMonday.map(p => new FormGroup({
        projectId: new FormControl(p?.id ?? null),
        input: new FormControl(null, [Validators.max(24)])
      }))),

      2 : new FormArray(projectsForTuesday.map(p => new FormGroup({
        projectId: new FormControl(p?.id ?? null),
        input: new FormControl(null, [Validators.max(24)])
      }))),

      3 : new FormArray(projectsForWednesday.map(p => new FormGroup({
        projectId: new FormControl(p?.id ?? null),
        input: new FormControl(null, [Validators.max(24)])
      }))),

      4 : new FormArray(projectsForThursday.map(p => new FormGroup({
        projectId: new FormControl(p?.id ?? null),
        input: new FormControl(null, [Validators.max(24)])
      }))),

      5 : new FormArray(projectsForFriday.map(p => new FormGroup({
        projectId: new FormControl(p?.id ?? null),
        input: new FormControl(null, [Validators.max(24)])
      })))
      
    })
  }

  didPressPrepareData(): void {
    this.form?.updateValueAndValidity(); 
    if(this.form?.valid) {
      const values = this.form.value;

      // convert the object of keys 1, 2, 3 ... to array based; each position contains a weekday, and within an array of projects
      const timeSpentMap = Object.keys(values).map((key: any) => values[key]);

      // at this stage the projects do not have a date bound to them; but it is possible to define one via the index on the []
      // e.g.: all projects for wednesday will be under timeSpentMap[3] (given monday is 1)
      const datedProadTimeRegsPerWeek: ProadTimeReg[] = timeSpentMap.map((projects, index) => {
        const weekDay = index + 1;
        const dateForProject = DateUtils.getDayFromWeekDayGivenLastWeekMonday(weekDay);

        return projects.map((p: { projectId: number, input: number }) => {

          const proadProjectMilestone = this.getProadMilestoneFromProjectId(p.projectId) ?? '';
          if(proadProjectMilestone && p.input > 0) {
            const proadTimeReg = new ProadTimeReg();
            proadTimeReg.from_date = format(dateForProject, 'yyyy-MM-dd');
            proadTimeReg.urno_project = p.projectId;
            proadTimeReg.input = p.input;
            proadTimeReg.urno_service_code = environment.proad.serviceCode;
            // proadTimeReg.urno_project = proadProjectMilestone; TODO: the urno_project is the actual ID from GET: proad/api/projects which we don't have access to atm
            proadTimeReg.urno_person = environment.proad.userId;
            return proadTimeReg;
          } else {
            return undefined; // if no PROAD project is set, user can't register
          }
          
        })

      });

      // TODO: these would be ready to batch POST to PROAD
      const allProadRecords = datedProadTimeRegsPerWeek.flat().filter(p => !!p);
      console.log(allProadRecords);
      
    }
  }

  getHoursSpentForProjectOnWeekday(projectId: number, weekday: number): number {
    const controlsForWeekday = this.form?.controls[weekday] as FormArray; // these are projects (formControls)
    const project = controlsForWeekday?.controls.find(c => c.value?.projectId === projectId);
    return project?.get('input')?.value ?? 0;
  }

  getTotalHoursSpendForProjectOnWeek(projectId: number): number {
    const hours = [1,2,3,4,5].reduce((prev, curr) => {
      const hours = this.getHoursSpentForProjectOnWeekday(projectId, curr);
      return prev + hours;
    }, 0);
    return hours;
  }

  didPressUpdateConfiguration(): void {

    const values: {[key: number]: string}[] = this.projectsForm?.value;
    if(values) {
      const proadProjectsMap: { gitlabProjectId: number, proadProjectMilestone: string }[] = Object.keys(values).map((key) => (
        {
          gitlabProjectId: parseInt(key),
          proadProjectMilestone: values[parseInt(key)].toString()
        }
      ));
      
      if(proadProjectsMap?.length > 0) {
        this.projectConfigService.save(proadProjectsMap);
      }
    }
    
  }

  didPressLogintoGithub(): void {
    console.log(this.gitlabApiClient.gitlabOAuthUri);
    window.location.href = this.gitlabApiClient.gitlabOAuthUri;
  }

}
