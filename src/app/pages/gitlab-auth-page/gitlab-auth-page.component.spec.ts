import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GitlabAuthPageComponent } from './gitlab-auth-page.component';

describe('GitlabAuthPageComponent', () => {
  let component: GitlabAuthPageComponent;
  let fixture: ComponentFixture<GitlabAuthPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GitlabAuthPageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GitlabAuthPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
