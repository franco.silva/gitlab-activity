import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GitlabApiClient } from 'src/app/modules/api-client/gitlab/gitlab-api-client';

@Component({
  selector: 'app-gitlab-auth-page',
  templateUrl: './gitlab-auth-page.component.html',
  styleUrls: ['./gitlab-auth-page.component.scss']
})
export class GitlabAuthPageComponent {

  constructor(
    private activatedRoute: ActivatedRoute,
    private gitlabClient: GitlabApiClient,
    private router: Router
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      if(params['code']) {
        this.gitlabClient.getAccessTokenFromCode(params['code']).subscribe((data: {
          access_token: string
          created_at: number 
          expires_in: number
          refresh_token: string
          scope: string
          token_type: string
        }) => {
          this.gitlabClient.setAccessToken(data.access_token);
          this.router.navigate([`/`]);
        });
      } else {

      }
    })
  }

}
