import { Component, Input } from '@angular/core';
import { GitlabEvent } from '../../api-client/gitlab/models/event';
import { GitlabProject } from '../../api-client/gitlab/models/project';

@Component({
  selector: 'app-events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.scss']
})
export class EventsListComponent {
  
  @Input()
  events: GitlabEvent[] = [];

  @Input()
  projects: GitlabProject[] = [];

  @Input()
  project?: GitlabProject;

  @Input()
  detailed = false;

  getActionLabelFromName(actionName: string): string {
    switch (actionName) {
      case 'pushed to':
      case 'pushed':
        return 'pushed some code';
      case 'commented':
        return 'commented on an issue';
      default:
        return actionName;
    }      
  }

  getProjectFromId(id: number): GitlabProject | undefined {
    return this.projects.find(p => p && p.id === id);
  }

}
