import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { StorageUtils } from 'src/app/utils/storage.utils';

@Injectable({
  providedIn: 'root'
})
export class ProjectConfigurationService {

  private localStorageKey = 'rm-gitlab-activity-config';
  private configuration = new BehaviorSubject<ProjectConfiguration[]>(this.read() || []);
  public configuration$ = this.configuration.asObservable();

  constructor(
    private storage: StorageUtils
  ) {}


  save(projects: ProjectConfiguration[]): void {
    this.storage.save(this.localStorageKey, (projects));
    this.configuration.next(projects);
  }

  read(): ProjectConfiguration[] | undefined{
    const data = this.storage.read(this.localStorageKey);
    return data ? (data) : undefined;
  }
}

export interface ProjectConfiguration { 
  gitlabProjectId: number;
  proadProjectMilestone: string;
}
