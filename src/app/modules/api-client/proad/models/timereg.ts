export class ProadTimeReg {
    urno_person?: number;
    urno_project?: number;
    urno_task?: number;
    urno_service_code?: string;
    from_date?: string; // TODO: PROAD expects string in YYYY-MM-DD format
    input?: number;
    description?: string;
    chargeable?: boolean; // TODO: PROAD expects 0 or 1
}