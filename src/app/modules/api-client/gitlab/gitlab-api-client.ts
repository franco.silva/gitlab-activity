import { HttpClient, HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, catchError, combineLatest, map, mergeMap, Observable, of, switchMap, throwError } from "rxjs";
import { ApiClientUtils } from "src/app/utils/api-client-utils";
import { StorageUtils } from "src/app/utils/storage.utils";
import { environment } from "src/environments/environment";
import { GitlabEvent } from "./models/event";
import { GitlabProject } from "./models/project";

@Injectable({
    providedIn: 'root'
})
export class GitlabApiClient implements HttpInterceptor {

    private accessToken$ = new BehaviorSubject<string | null>(null);
    private perPage = 200;
    private localStorageKey = 'rm-gitlab-access-token';

    get accessToken(): string | null {
        return this.accessToken$.getValue();
    }

    get apiUrl(): string {
        return 'https://gitlab.rabbit-mobile.de/api/v4/';
    }

    get gitlabOAuthUri(): string {
        // TODO: csrf 
        return `https://gitlab.rabbit-mobile.de/oauth/authorize?client_id=${environment.gitlab.oAuthappId}&redirect_uri=${environment.gitlab.oAuthRedirectUrl}&response_type=code&scope=api+read_user+read_repository`;
    }

    get hasAccessToken() {
        return this.accessToken$.getValue() ? true : false;
    }

    constructor(
        private http: HttpClient,
        private storage: StorageUtils
    ) {

        // check if a token exists in storage
        if(this.storage.read(this.localStorageKey)) {
            this.accessToken$.next(this.storage.read(this.localStorageKey));
        }

        // keep track of changes to access token and save to storage
        this.accessToken$.subscribe(token => {
            if(token) {
                this.storage.save(this.localStorageKey, token)
            }
        })
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            catchError((err: any) => {
                if(err.error.error_description === 'Token is expired. You can either do re-authorization or token refresh.') {
                    this.removeAccessToken();
                }
                console.log('err');
                console.log(err);
                return throwError(() => new Error(err))
            })
        );
    }

    getUserEvents(userId: number, params: {[key: string]: any} = {}): Observable<GitlabEvent[]> {

        // predefine params
        params['per_page'] = this.perPage;
        params['access_token'] = this.accessToken;
        
        const endpoint = `${this.apiUrl}/users/${userId}/events${ApiClientUtils.toQueryString(params)}`;
        return this.http.get(endpoint).pipe(
            map((events: any) => {
                return events.map((e: any) => {
                    const event = new GitlabEvent();
                    Object.assign(event, e);
                    return event;
                })
            })
        );
    }

    getProject(projectId: number): Observable<GitlabProject> {
        const endpoint = `${this.apiUrl}/projects/${projectId}?access_token=${this.accessToken}&per_page=${this.perPage}`;
        return this.http.get(endpoint).pipe(
            map((p: any) => {
                const project = new GitlabProject();
                Object.assign(project, p)
                return project;
            })
        );
    }

    getProjects(projectIds: number[]): Observable<GitlabProject[]> {
        return combineLatest(projectIds.map(id => this.getProject(id)));
    }

    getUserActivity(userId: number, params?: {[key: string]: any}): Observable<[GitlabEvent[], GitlabProject[]]> {

        // get user activity
        return this.getUserEvents(userId, params).pipe(
            // combine single request into request + individual project requests
            switchMap(events => {
                const projectIds = [...new Set(events.map(event => event.project_id))].filter((p): p is number => !!p);
                return combineLatest([
                    of(events),
                    combineLatest(
                        projectIds.map(pId => this.getProject(pId))
                    )
                ])
            })
        )

    }

    getAccessTokenFromCode(code: string): Observable<any> {

        const body = {
            client_id: environment.gitlab.oAuthappId,
            code,
            grant_type: 'authorization_code',
            redirect_uri: 'http://localhost:4200/gitlab_success'
        };
        
        const endpoint = `https://gitlab.rabbit-mobile.de/oauth/token`;
        return this.http.post(endpoint, body)

    }

    setAccessToken(token: string): void {
        this.accessToken$.next(token);
    }

    removeAccessToken(): void {
        this.accessToken$.next(null);
        this.storage.delete(this.localStorageKey);
    }

}