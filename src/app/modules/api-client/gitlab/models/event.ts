import { GitlabEventAuthor } from "./author";

export class GitlabEvent {
    id?: number;
    project_id?: number;
    action_name?: string;
    target_id?: number;
    target_iid?: number;
    target_type?: string;
    author_id?: number;
    target_title?: string;
    created_at?: string;
    note?: {
      id: number;
      type: string;
      body: string;
      author: GitlabEventAuthor,
      created_at: string;
      updated_at: string;
      noteable_iid: number
    };
    author?: GitlabEventAuthor
    push_data? : {
      commit_title: string;
    }
}
