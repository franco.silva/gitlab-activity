export class GitlabProject {
    id?: number;
    description?: string;
    name: string = '';
    name_with_namespace?: string;
    web_url?: string;
    readme_url?: string;
}