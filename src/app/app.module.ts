import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { LOCALE_DE_DE_DATES, RM_UI_DATE_LOCALE, RmUiModule } from '@rabbit-mobile/rm-ui';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { GitlabAuthPageComponent } from './pages/gitlab-auth-page/gitlab-auth-page.component';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { ApiClientModule } from './modules/api-client/gitlab/api-client.module';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/en';
import { EventsListComponent } from './modules/shared/events-list/events-list.component';
import { MarkdownModule } from 'ngx-markdown';

registerLocaleData(localeDe, 'en-EN');

@NgModule({
  declarations: [
    AppComponent,
    GitlabAuthPageComponent,
    MainPageComponent,
    EventsListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RmUiModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    ApiClientModule,
    MarkdownModule.forRoot()
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'en-EN' },
    { provide: RM_UI_DATE_LOCALE, useValue: LOCALE_DE_DE_DATES },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
