// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  gitlab: {
    userId: 19,
    // accessToken: 'glpat-7GeU4zqLEZwwK4eEAMpG',
    oAuthappId: '3ba2eedda6d70d9c277fae0cad147c95557c04ca7d3d6a384f1c0bb9dd3dec46',
    oAuthRedirectUrl: 'http://localhost:4200/gitlab_success'
  },
  proad: {
    userId: 0,
    serviceCode: 'PR02'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
